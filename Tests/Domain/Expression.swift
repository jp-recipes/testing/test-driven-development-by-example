//
//  Expression.swift
//  Tests
//
//  Created by JP on 27-08-23.
//

import Foundation

protocol Expression {
    func reduce(bank: Bank, to: String) -> Money
    func plus(addend: Expression) -> Expression
    func times(multiplier: Int) -> Expression
}
