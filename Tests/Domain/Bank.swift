//
//  Bank.swift
//  Tests
//
//  Created by JP on 27-08-23.
//

import Foundation

class Bank {
    private var rates: [Pair: Int] = [:]

    func addRate(from: String, to: String, rate: Int) {
        rates[Pair(from: from, to: to)] = rate
    }

    func rate(from: String, to: String) -> Int {
        if from == to { return 1 }

        return rates[Pair(from: from, to: to)] ?? 0
    }

    func reduce(source: Expression, to: String) -> Money {
        source.reduce(bank: self, to: to)
    }
}
