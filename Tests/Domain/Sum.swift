//
//  Sum.swift
//  Tests
//
//  Created by JP on 27-08-23.
//

import Foundation

struct Sum: Expression {
    let augend: Expression
    let addend: Expression

    func plus(addend: Expression) -> Expression {
        Sum(augend: self, addend: addend)
    }

    func times(multiplier: Int) -> Expression {
        Sum(augend: augend.times(multiplier: multiplier), addend: addend.times(multiplier: multiplier))
    }

    func reduce(bank: Bank, to: String) -> Money {
        let amount = augend.reduce(bank: bank, to: to).amount + addend.reduce(bank: bank, to: to).amount
        return Money(amount: amount, currency: to)
    }
}
