//
//  Money.swift
//  Tests
//
//  Created by JP on 27-08-23.
//

import Foundation

struct Money: Expression, Equatable {
    let amount: Int
    let currency: String

    func plus(addend: Expression) -> Expression {
        Sum(augend: self, addend: addend)
    }

    func times(multiplier: Int) -> Expression {
        Money(amount: amount * multiplier, currency: currency)
    }

    func reduce(bank: Bank, to: String) -> Money {
        let rate = bank.rate(from: currency, to: to)
        return Money(amount: amount / rate, currency: to)
    }
}

extension Money {
    static func dollar(_ amount: Int) -> Money {
        Money(amount: amount, currency: "USD")
    }

    static func franc(_ amount: Int) -> Money {
        Money(amount: amount, currency: "CHF")
    }
}
