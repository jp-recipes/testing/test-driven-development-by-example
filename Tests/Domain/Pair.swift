//
//  Pair.swift
//  Tests
//
//  Created by JP on 27-08-23.
//

import Foundation

struct Pair: Hashable, Equatable {
    let from: String
    let to: String
}
