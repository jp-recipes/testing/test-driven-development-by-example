//
//  Tests.swift
//  Tests
//
//  Created by JP on 26-08-23.
//

import XCTest

final class MoneyConversionTests: XCTestCase {
    func testMultiplication() {
        let five = Money.dollar(5)

        XCTAssertEqual(Money.dollar(10), five.times(multiplier: 2) as! Money)
        XCTAssertEqual(Money.dollar(20), five.times(multiplier: 4) as! Money)
    }

    func testEquality() {
        XCTAssertEqual(Money.dollar(5), Money.dollar(5))
        XCTAssertNotEqual(Money.dollar(5), Money.dollar(6))
        XCTAssertNotEqual(Money.dollar(1), Money.franc(1))
    }

    func testCurrency() {
        XCTAssertEqual("CHF", Money.franc(1).currency)
        XCTAssertEqual("USD", Money.dollar(1).currency)
    }

    func testSimpleAddition() {
        let five = Money.dollar(5)
        let sum = five.plus(addend: five)
        let bank = Bank()

        let reduced = sum.reduce(bank: bank, to: "USD")

        XCTAssertEqual(Money.dollar(10), reduced)
    }

    func testPlusReturnsSum() {
        let five = Money.dollar(5)

        let sum = five.plus(addend: five) as! Sum

        XCTAssertEqual(five, sum.augend as! Money)
        XCTAssertEqual(five, sum.addend as! Money)
    }

    func testRedeuceSum() {
        let sum = Sum(augend: Money.dollar(3), addend: Money.dollar(4))
        let bank = Bank()

        let result = bank.reduce(source: sum, to: "USD")

        XCTAssertEqual(Money.dollar(7), result)
    }


    func testReduceMoney() {
        let bank = Bank()

        let result = bank.reduce(source: Money.dollar(1), to: "USD")

        XCTAssertEqual(Money.dollar(1), result)
    }

    func testReduceMoneyDifferentCurrency() {
        let bank = Bank()
        bank.addRate(from: "CHF", to: "USD", rate: 2)

        let result = bank.reduce(source: Money.franc(2), to: "USD")

        XCTAssertEqual(Money.dollar(1), result)
    }

    func testIdentityRate() {
        XCTAssertEqual(1, Bank().rate(from: "USD", to: "USD"))
    }

    func testMixedAddition() {
        let fiveBucks = Money.dollar(5)
        let tenFrancs = Money.franc(10)
        let bank = Bank()
        bank.addRate(from: "CHF", to: "USD", rate: 2)

        let sum = Sum(augend: fiveBucks, addend: tenFrancs).plus(addend: fiveBucks)
        let result = bank.reduce(source: sum, to: "USD")

        XCTAssertEqual(Money.dollar(15), result)
    }

    func testSumPlusMoney() {
        let fiveBucks = Money.dollar(5)
        let tenFrancs = Money.franc(10)
        let bank = Bank()
        bank.addRate(from: "CHF", to: "USD", rate: 2)

        let multiplication = Sum(augend: fiveBucks, addend: tenFrancs).times(multiplier: 2)
        let result = bank.reduce(source: multiplication, to: "USD")

        XCTAssertEqual(Money.dollar(20), result)
    }
}
